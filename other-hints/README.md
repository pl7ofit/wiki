### Установка docker для Ubuntu 20.04

- **Копируем в терминал всё целиком(под рутом)**

      apt-get remove docker docker-engine docker.io containerd runc || true  && \
      apt-get update  && \
      apt-get install -y  apt-transport-https ca-certificates curl gnupg-agent software-properties-common && \
      curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -  && \
      apt-key fingerprint 0EBFCD88  && \
      add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu  focal stable"  && \
      apt-get update  && \
      apt-get install -y docker-ce docker-ce-cli containerd.io

- **Добавляем обычного пользователя в группу docker**

      addgroup ubuntu docker

- **Перезапускаем сервисы docker**

      systemctl restart containerd.service
      systemctl restart docker.service

- **Выходим из рута и запускаем hello-world контейнер под обычным пользователем для проверки**

      docker run hello-world
_если всё хорошо должно вывести что-то вроде hello from docker!_

### Установка docker-compose для Ubuntu 20.04

- **Копируем в терминал всё целиком(под рутом)**

      wget -qO- \
      "https://github.com/docker/compose/releases/download/1.27.4/docker-compose-$(uname -s)-$(uname -m)" > \
      /usr/local/bin/docker-compose  && \
      chmod +x /usr/local/bin/docker-compose  && \
      ln -s /usr/local/bin/docker-compose /usr/bin/docker-compose 


- **Проверяем что docker-compose работает**

      docker-compose --version

_если всё хорошо то мы увидим версию docker-compose_

### Установка nginx для ubuntu 20.04

- **Копируем в терминал всё целиком(под рутом)**

      add-apt-repository ppa:nginx/stable -y -u && sudo apt install -y nginx
      nginx -v
_если видим последнюю версию nginx значит всё круто_

### Установка go-ethereum из официального репозитория

- **Добавляем репозиторий**

      add-apt-repository -y -u ppa:ethereum/ethereum 
- **Устанавливаем**

      apt-get install -y ethereum
- **Проверяем версию**

      geth version






### Скачивание и распаковка tar.gz на лету с усечением первых двух директорий
-
      wget -qO - "https://site.com/archive.tar.gz" | dd status=progress | tar -xzf - --strip-components 2 


### Скачивание и распаковка zip на лету(требуется bsdtar)
-
      wget -qO - "https://site.com/archive.zip" | dd status=progress  | bsdtar -xf-




### Пример bsc ноды в одном docker-compose файле
-
      version: '3.5'
      networks:
        default:
          name: geth_bsc_network
          driver: bridge
      services:
        geth-bsc:
          container_name: geth-bsc
          image: ubuntu:focal
          environment:
            - _DEPS=wget unzip
            - _BIN_URL=https://github.com/binance-chain/bsc/releases/download/v1.1.0-beta/geth_linux
            - _NET_URL=https://github.com/binance-chain/bsc/releases/download/v1.1.0-beta/testnet.zip
            - _INIT=./geth init genesis.json --datadir /root/.ethereum
            - _RUN=./geth --datadir /root/.ethereum --verbosity 5  --txlookuplimit 0  --allow-insecure-unlock --rpc.allow-unprotected-txs  --config config.toml  --syncmode light --cache=512 --maxpeers=4000 --http --http.addr 0.0.0.0 --http.port 8545 --http.corsdomain '*' --http.vhosts '*'  --http.api 'rpc, debug, admin, eth, net, web3, personal, txpool' --ws  --ws.addr  0.0.0.0 --ws.port 8546  --ws.origins '*' --ws.api 'rpc, debug, admin, eth, net, web3, personal, txpool'
          volumes:
            - './node_data:/root/.ethereum'
          ports:
            - 0.0.0.0:8545:8545
            - 0.0.0.0:8546:8546
            - 0.0.0.0:30303:30303
          command: |
            bash -c '
                     set -eu ;
                     apt update && apt install $$_DEPS -y  ;
                     mkdir -p /root/bsc && cd /root/bsc ;
                     wget -q $$_BIN_URL  -O geth ;
                     wget -q $$_NET_URL ;
                     unzip -o testnet.zip  ; 
                     printf "
                             touch /root/.ethereum/tmp.log \n 
                             tail -f /root/.ethereum/*.log & \n 
                             if [ ! -f /root/.ethereum  ]; 
                             then 
                               $$_INIT; 
                             fi \n
                             $$_RUN
                             " > run_node.sh ;
                    chmod +x run_node.sh geth ;
                    cat run_node.sh ;
                    ./run_node.sh '


### Пример ethereum ноды в одном docker-compose файле
-
      version: '3.9'
      networks:
        default:
          name: geth_eth_network
          driver: bridge

      services:
        geth-bsc:
          container_name: geth-eth
          image: ethereum/client-go:v1.10.6
          entrypoint: ""
          command: |
            sh -c "geth --datadir /root/.ethereum --rinkeby --verbosity 3  --txlookuplimit 0  --allow-insecure-unlock --rpc.allow-unprotected-txs  --syncmode snap --cache=500 --maxpeers=4000 --http --http.addr 0.0.0.0 --http.port 8545 --http.corsdomain '*' --http.vhosts '*'  --http.api 'rpc, eth, net, web3, personal, txpool' --ws  --ws.addr  0.0.0.0 --ws.port 8546  --ws.origins '*' --ws.api 'rpc, eth, net, web3, personal, txpool'"
          volumes:
            - './node_data:/root/.ethereum'
          ports:
            - 0.0.0.0:8545:8545
            - 0.0.0.0:8546:8546
            - 0.0.0.0:30303:30303



### Быстрое создание gitlab раннера
-
      gitlab-runner register --non-interactive \
            --tag-list "build, deploy,test,lint" \
            --executor "docker" \
            --docker-image docker:20.10 \
            --locked=false \
            --docker-volumes "/cache"
            --docker-volumes "/var/run/docker.sock:/var/run/docker.sock" \
            --url "https://git.sfxdx.ru/" \
            --registration-token "TOKEN" \
            --description "New_project_gitlab_runner" 

### Pipe фильтр цветовых кодов bash
-
      sed 's/\x1b\[[0-9;]*m//g'

### Пример hexdump для поиска плохих символов в файле
-
      hexdump -e '16/1 "%03i ""\t"" "' -e '16/1 "%_p""\n"' test.txt

### Просмотр всех авторов и коммитеров репозитория
-
      git shortlog -snc --all

### Перезапись всех авторов и коммитеров
(achtung!) меняет хэши, требует форспуш!

-
      git filter-branch -f --env-filter "
          GIT_AUTHOR_NAME='IntegralTeam'
          GIT_AUTHOR_EMAIL='repo@sfxdx.ru>'
          GIT_COMMITTER_NAME='IntegralTeam'
          GIT_COMMITTER_EMAIL='repo@sfxdx.ru>'
        " HEAD

### Прокидывание локальных портов на удалённый сервер
-
      ssh -R 8545:localhost:8545 -R 8546:localhost:8546 root@123.123.123.123 -N
